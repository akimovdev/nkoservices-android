package ru.nko.nkoservices;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import ru.nko.nkoservices.components.BitmapUtil;

public class UploadFileService extends Service {

    //private  static final String FILE_STORAGE = "http://s673212913.onlinehome.us/nkoserver/sandbox/fileupload/uploads/";

    public static final String ON_PROGRESS_ACTION = "ru.nko.nkoservices.action.ON_PROGRESS";

    public static final int RECEIVE_PROGRESS = 1;
    public static final int RECEIVE_SUCCESS = 2;
    public static final int RECEIVE_FAIL = 3;

    //private String filePath;
    BitmapUtil bitmapUtil;
    Uri imageUri;
    private long serviceID;
    private long totalSize = 0;
    private String imageName;
    private String savedName;

    @Override
    public void onCreate() {
        super.onCreate();

        bitmapUtil = new BitmapUtil();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent != null) {
            Log.d("UPLOAD_FILE_SERVICE", "Fine");
            imageUri = intent.getParcelableExtra(Constants.Extras.IMAGE_URI);

            serviceID = intent.getLongExtra(Constants.Extras.SERVICE_ID, -1);
            new UploadFileToServer().execute();
        } else {
            Log.d("UPLOAD_FILE_SERVICE", "NULL");
        }



        return super.onStartCommand(intent, flags, startId);
    }


    private void sendResultBroadcast(int resultCode) {
        Intent intent = new Intent();
        intent.setAction(ON_PROGRESS_ACTION);
        intent.putExtra(Constants.Extras.RECEIVE, resultCode);
        intent.putExtra(Constants.Extras.FILE_PATH, Constants.FILE_STORAGE+savedName+".jpg");
        sendBroadcast(intent);
    }

    private void sendProgressBroadcast(int progress) {
        Intent intent = new Intent();
        intent.setAction(ON_PROGRESS_ACTION);
        intent.putExtra(Constants.Extras.RECEIVE, RECEIVE_PROGRESS);
        intent.putExtra(Constants.Extras.PROGRESS, progress);
        sendBroadcast(intent);
    }

    private static final int MAX_IMAGE_DIMENSION = 720;

    protected byte[] compress(Uri uriFile) {

        byte[] bytes = new byte[0];

        try {
            //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFile);
            Bitmap bitmap = bitmapUtil.decodeSampledBitmapFromResourceNEW2(this, uriFile, 720, 480);

            int width = bitmap.getWidth();

            if (width > MAX_IMAGE_DIMENSION) {
                float ratio = (float) MAX_IMAGE_DIMENSION / (float) width;

                width = MAX_IMAGE_DIMENSION;
                int height = (int)((float)bitmap.getHeight() * ratio);

                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            bitmap.recycle();
            stream.flush();
            stream.close();
            bytes = stream.toByteArray();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return bytes;
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            sendProgressBroadcast(progress[0]);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return uploadFile();
        }

        @Override
        protected void onPostExecute(Integer result) {
            sendResultBroadcast(result);
            super.onPostExecute(result);
            stopSelf();
        }

        @SuppressWarnings("deprecation")
        private Integer handleProgress() throws IOException {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constants.FILE_UPLOAD_URL);

            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                    new AndroidMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });

            //File sourceFile = new File(filePath);
            savedName = String.valueOf(System.currentTimeMillis());

            //Bitmap bmp = BitmapFactory.decodeFile(filePath);
            //ByteArrayOutputStream bos = new ByteArrayOutputStream();
            //bmp.compress(Bitmap.CompressFormat.JPEG, 20, bos);

            InputStream in = new ByteArrayInputStream(compress(imageUri));
            ContentBody foto = new InputStreamBody(in, "image/jpeg", savedName);


            //entity.addPart("image", new FileBody(sourceFile));
            entity.addPart("image", foto);
            entity.addPart("name", new StringBody(savedName));



            totalSize = entity.getContentLength();
            httppost.setEntity(entity);

            HttpResponse response = httpclient.execute(httppost);
            int statusCode = response.getStatusLine().getStatusCode();


            if (statusCode != 200)
                return RECEIVE_FAIL;

            return RECEIVE_SUCCESS;
        }

        private Integer uploadFile() {
            try {
                return handleProgress();
            } catch (IOException e) {
                return RECEIVE_FAIL;
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
