package ru.nko.nkoservices;


import java.util.HashMap;
import java.util.Map;

public class Container {
    private static Container instance = null;
    private static Map<String, Object> map = new HashMap<>();

    private Container() {}

    public static void initialize() {

        if (instance != null)
            throw new Error("Container has been initialized !!!");

        instance = new Container();
    }

    public static Container getInstance() {
        if (instance == null)
            throw new Error("Container has not been initialized. Please call initialize in onCreate of Application class !!!");

        return instance;
    }

    public String push(Object object) {
        String key = String.valueOf(System.currentTimeMillis());
        map.put(key, object);
        return key;
    }

    public Object pop(String key) {
        return map.remove(key);
    }
}
