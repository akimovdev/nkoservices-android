package ru.nko.nkoservices;

public final class Constants {
    public static final String APP_PREFERENCES = "app_preferences";
    public static final String DEBUG_PHONE = "999 999 88 55";
    public static final String SYMBOL_RUB = " ₽";
    public static final String FILE_UPLOAD_PRODUCTION = "http://navigatorsocuslug.ru/fileupload/fileUpload.php";
    public static final String FILE_UPLOAD_SANDBOX = "http://s673212913.onlinehome.us/nkoserver/sandbox/fileupload/fileUpload.php";

    public static final String FILE_STORAGE = "http://navigatorsocuslug.ru/fileupload/uploads/";
    public static final String FILE_UPLOAD_URL = FILE_UPLOAD_PRODUCTION;
    public static final String SERVER_PRODUCTION = "http://navigatorsocuslug.ru/";

    // JSON RESPONSES
    public static final int STATUS_SUCCESSFUL = 1;
    public static final int STATUS_FAIL = 0;
    public static final String USER = "user";
    public static final String STATUS = "status";
    public static final String RESULT = "result";
    public static final String RESULTS = "results";
    public static final String GEOMETRY = "geometry";
    public static final String LOCATIONS = "locations";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String MESSAGE = "message";
    public static final String ERROR = "error";
    public static final String CATEGORIES = "categories";
    public static final String LOCATION = "location";
    public static final String SERVICES = "services";
    public static final String SERVICE = "service";

    public static class Extras {
        public static  final String CONTAINER_CATEGORIES = "container_categories";
        public static  final String CONTAINER_SERVICE = "container_service";
        public static  final String CONTAINER_MODEL = "container_model";
        public static  final String START_MODE = "start_mode";
        public static  final String SERVICE_ID = "service_id";
        public static  final String CATEGORY_ID = "category_id";
        public static  final String LOCATION_ID = "location_id";
        public static  final String CATEGORY_NAME = "category_name";
        public static  final String LOCATION_NAME = "location_name";
        public static  final String LOCATION_AREA = "location_area";
        public static  final String IMAGE_URI = "image_uri";
        public static  final String PROGRESS = "progress";
        public static  final String RECEIVE = "receive";
        public static  final String AUTH_RESULT = "auth_result";
        public static  final String MODE = "mode";
        public static  final String FILE_PATH = "file_path";
    }

    public static class Mode {
        public static final int SELECT_CATEGORY = 1;
        public static final int OPEN_CATEGORY = 2;

    }
}
