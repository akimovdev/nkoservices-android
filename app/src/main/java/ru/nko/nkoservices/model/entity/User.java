package ru.nko.nkoservices.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

@Table(name = "User")
public class User extends Model {

    public static final int STATE_USER = 2;
    public static final int STATE_PERFORMER = 1;



    @SerializedName("id")
    @Column(name = "ServerID")
    private long serverID;

    @Column(name = "Name")
    private String name;

    @SerializedName("phone")
    @Column(name = "PhoneNumber")
    private String phoneNumber;

    @Column(name = "Email")
    private String email;

    @SerializedName("state_id")
    @Column(name = "State")
    private int state;

    private String accreditation;

    private String experiance;

    private String leader;

    @Column(name = "Password")
    private String password;

    public static User createUser() {
        User user = new User();
        user.state = STATE_USER;
        return user;
    }

    public static User createPerformer() {
        User user = new User();
        user.state = STATE_PERFORMER;
        return user;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phoneNumber = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setExperiance(String experiance) {
        this.experiance = experiance;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getServerID() {
        return serverID;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getExperiance() {
        return experiance;
    }

    public String getPhone() {
        return phoneNumber;
    }

    public int getState() { return state; }

    public String getPassword() { return password; }

    public void setAccreditation(String accreditation) {
        this.accreditation = accreditation;
    }

    public String getAccreditation() {
        return accreditation;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getLeader() {
        return leader;
    }
}
