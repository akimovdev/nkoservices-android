package ru.nko.nkoservices.model.serialization;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import ru.nko.nkoservices.model.entity.Service;

public class ServiceDeserializer implements JsonDeserializer<Service> {

    @Override
    public Service deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonService = json.getAsJsonObject();

        Service service = new Service();
        service.setServerID(jsonService.get("id").getAsLong());
        service.setName(jsonService.get("name").getAsString());



        return service;
    }
}
