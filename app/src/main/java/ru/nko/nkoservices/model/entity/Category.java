package ru.nko.nkoservices.model.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Table(name = "Category")
public class Category extends Model {

    @SerializedName("id")
    @Column(name = "serverID")
    private long id = -1;

    @Column(name = "Name")
    private String name;

    @Column(name = "Children")
    private List<Category> children;

    public void setServerId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public long getServerID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Category> getChildren() {
        return children;
    }
}
