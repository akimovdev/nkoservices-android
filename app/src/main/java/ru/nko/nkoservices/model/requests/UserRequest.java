package ru.nko.nkoservices.model.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.model.entity.User;

public class UserRequest extends StringRequest {

    private static final int ERROR_EMAIL_EXISTS = 0;
    //public static int[] ERROR_MESSAGE_RESOURCE = new int[] { R.string.email_already_exists,};

    private Map<String, String> params = new HashMap<String, String>();

    private UserRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    private UserRequest(int method, String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.params.putAll(params);
    }

    public static UserRequest login(String email, String password, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("email", email);
        params.put("password", password);

        return new UserRequest(Method.POST, Constants.SERVER_PRODUCTION+"users/restLogin.json", params, listener, errorListener);
        //return new UserRequest(Method.POST, "http://s673212913.onlinehome.us/nkoserver/sandbox/app/users/restLogin.json", params, listener, errorListener);
    }

    public static UserRequest register(User user, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("email", user.getEmail());
        params.put("name", user.getName());
        params.put("password", user.getPassword());
        params.put("state_id", String.valueOf(user.getState()));
        params.put("phone", String.valueOf(user.getPhone()));

        //params.put("password", user.getPassword());

        //return new UserRequest(Method.POST, "http://s673212913.onlinehome.us/nkoserver/sandbox/app/users/restAdd.json", params, listener, errorListener);
        return new UserRequest(Method.POST, Constants.SERVER_PRODUCTION+"users/restAdd.json", params, listener, errorListener);
    }

    public static UserRequest requestRecoverPassword(String email, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //return new UserRequest(Method.GET, "http://s673212913.onlinehome.us/nkoserver/sandbox/app/users/restEmailRecover/"+email+".json", listener, errorListener);
        return new UserRequest(Method.GET, Constants.SERVER_PRODUCTION+"users/restEmailRecover/"+email+".json", listener, errorListener);
    }

    public static UserRequest requestOrderPerfo(User user, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("email", user.getEmail());
        params.put("name", user.getName());
        params.put("password", user.getPassword());
        params.put("phone", String.valueOf(user.getPhone()));
        params.put("leader", String.valueOf(user.getLeader()));
        params.put("experience", String.valueOf(user.getExperiance()));
        params.put("accreditation", String.valueOf(user.getAccreditation()));
        params.put("password", String.valueOf(user.getPassword()));

        //return new UserRequest(Method.POST, "http://s673212913.onlinehome.us/nkoserver/sandbox/app/users/restOrderPerformer.json", params, listener, errorListener);
        return new UserRequest(Method.POST, Constants.SERVER_PRODUCTION+"users/restOrderPerformer.json", params, listener, errorListener);
    }

    public static UserRequest requestCallNotification(User user, Service service, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("name", user.getName());
        params.put("phone", String.valueOf(user.getPhone()));
        params.put("performerName", service.getPerformer().getName());
        params.put("serviceName", service.getName());

        //return new UserRequest(Method.POST, "http://s673212913.onlinehome.us/nkoserver/sandbox/app/users/restCallNotification.json", params, listener, errorListener);
        return new UserRequest(Method.POST, Constants.SERVER_PRODUCTION+"users/restCallNotification.json", params, listener, errorListener);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }

    public static int getMessageResource(int code) {

        if (code == ERROR_EMAIL_EXISTS)
            return R.string.email_already_exists;

        return R.string.not_recognised_server_error;
    }

}
