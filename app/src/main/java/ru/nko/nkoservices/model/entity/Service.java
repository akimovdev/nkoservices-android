package ru.nko.nkoservices.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Service {

    public final static int TYPE_FREE = 1;
    public final static int TYPE_PAY = 2;

    @SerializedName("id")
    private long serverID;

    private String name;

    @SerializedName("price")
    private int cost;

    @SerializedName("type_id")
    private int type;

    private User user;

    @SerializedName("description")
    private String overview;

    @Expose
    private List<Image> images = new ArrayList<>();

    private Location location;

    private Category category;

    public void setTypeID(int type) {
        this.type = type;
    }

    public int getTypeID() {
        return type;
    }

    public void setServerID(long serverID) {
        this.serverID = serverID;
    }

    public long getServerID() {
        return serverID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOverview() {
        return overview;
    }


    public void setPerformer(User user) {
        this.user = user;
    }

    public User getPerformer() {
        return user;
    }

    public void addImageUrl(String url) {
        Image image = new Image();
        image.setUrl(url);
        this.images.add(image);
    }

    public void addImages(List<Image> images) {
        this.images.addAll(images);
    }

    public List<Image> getImages() {

        //images = new ArrayList<>();
        //images.add("http://www.vseodetyah.com/editorfiles/vygul-sobaki-03.jpg");
        //images.add("http://www.infpol.ru/upload/resize_cache/iblock/323/1200_600_1/3231584aa6c329d6a973b900ef401a52.jpg");
        //images.add("http://mybullterrier.ru/wp-content/uploads/2016/03/16031301.jpg");

        return images;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}

