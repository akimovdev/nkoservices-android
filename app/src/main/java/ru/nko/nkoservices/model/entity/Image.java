package ru.nko.nkoservices.model.entity;

import com.google.gson.annotations.SerializedName;

public class Image {

    private String url;

    @SerializedName("service_id")
    private long serviceID;

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() { return url; }

    public void setServiceID(long serviceID) {
        this.serviceID = serviceID;
    }

    public long getServiceID() {
        return serviceID;
    }
}
