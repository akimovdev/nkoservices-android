package ru.nko.nkoservices.model.requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;

public class GeocodingRequest extends StringRequest {

    public final static String RESPONSE_OK = "OK";


    private GeocodingRequest (int method, String url, /*Map<String, String> params,*/ Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        //this.params.putAll(params);
    }

    public static GeocodingRequest requestListLocations(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=" + NKOApplication.getInstance().getString(R.string.google_maps_key);

        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/locations/restIndex.json";
        String path = Constants.SERVER_PRODUCTION+"locations/restIndex.json";
        return new GeocodingRequest (Method.GET, path, listener, errorListener);
    }


    public static GeocodingRequest requestLocationAddressBy(String address, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String query = "address="+address+"&key=" + NKOApplication.getInstance().getString(R.string.google_maps_key);
        //String path = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=" + NKOApplication.getInstance().getString(R.string.google_maps_key);


        try {

            //String path = "https://maps.googleapis.com/maps/api/geocode/json?address="+URLEncoder.encode(address, "utf-8")+"&key=" + NKOApplication.getInstance().getString(R.string.google_maps_key);
            String path = "https://maps.googleapis.com/maps/api/geocode/json?address="+URLEncoder.encode(address, "utf-8")+"&key=" + NKOApplication.getInstance().getString(R.string.google_maps_key);
            return new GeocodingRequest (Method.GET, path, listener, errorListener);
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            throw new Error("Cant parse");
        }
    }

}
