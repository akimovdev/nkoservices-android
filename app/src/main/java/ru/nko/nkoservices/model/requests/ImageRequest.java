package ru.nko.nkoservices.model.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.model.entity.Image;

public class ImageRequest extends StringRequest {

    private Map<String, String> params = new HashMap<String, String>();

    public ImageRequest(int method, String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.params.putAll(params);
    }

    public static ImageRequest requestAddImage(Image image, Response.Listener<String> listener, Response.ErrorListener errorListener) {


        Map<String, String> params = new HashMap<String, String>();

        params.put("url", image.getUrl());
        params.put("service_id", String.valueOf(image.getServiceID()));

        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/images/restAdd.json";
        String path = Constants.SERVER_PRODUCTION+"images/restAdd.json";
        return new ImageRequest(Method.POST, path, params, listener, errorListener);

    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }

}
