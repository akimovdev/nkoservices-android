package ru.nko.nkoservices.model.requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import ru.nko.nkoservices.Constants;

public class CategoryRequest extends StringRequest{

    public CategoryRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public static CategoryRequest loadCategories(Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //return new CategoryRequest(Method.GET, "http://s673212913.onlinehome.us/nkoserver/category", listener, errorListener);
        //return new CategoryRequest(Method.GET, "http://s673212913.onlinehome.us/nkoserver/sandbox/app/categories/restIndex.json", listener, errorListener);
        return new CategoryRequest(Method.GET, Constants.SERVER_PRODUCTION+"categories/restIndex.json", listener, errorListener);
    }
}
