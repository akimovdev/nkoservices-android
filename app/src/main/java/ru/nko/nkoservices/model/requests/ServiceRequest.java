package ru.nko.nkoservices.model.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.model.entity.Service;


public class ServiceRequest extends StringRequest {

    private Map<String, String> params = new HashMap<String, String>();

    private ServiceRequest(int method, String url, Map<String, String> params, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
        this.params.putAll(params);
    }

    private ServiceRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    public static ServiceRequest requestAddService(Service service, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/services/restAdd.json";
        String path = Constants.SERVER_PRODUCTION+"services/restAdd.json";
        return requestWriteService(service, path, listener, errorListener);
    }

    public static ServiceRequest requestEditService(Service service, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/services/restEdit/"+service.getServerID()+".json";
        String path = Constants.SERVER_PRODUCTION+"services/restEdit/"+service.getServerID()+".json";
        return requestWriteService(service, path, listener, errorListener);
    }

    private static ServiceRequest requestWriteService(Service service, String path, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("name", service.getName());
        params.put("price", String.valueOf(service.getCost()));
        params.put("description", service.getOverview());
        params.put("user_id", String.valueOf(service.getPerformer().getServerID()));
        params.put("type_id", String.valueOf(service.getTypeID()));
        params.put("location_id", String.valueOf(service.getLocation().getServerID()));
        params.put("category_id", String.valueOf(service.getCategory().getServerID()));
        //params.put("category_id", String.valueOf(1));
        params.put("public", "0");


        return new ServiceRequest(Method.POST, path, params, listener, errorListener);
    }

    public static ServiceRequest requestInstanceIdBy(long id, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/services/restView/"+id+".json";
        String path = Constants.SERVER_PRODUCTION+"services/restView/"+id+".json";
        return new ServiceRequest(Method.POST, path, listener, errorListener);
    }

    public static ServiceRequest requestListOwnerBy(long ownerID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/services/restIndexOwnerBy/"+ownerID+".json";
        String path = Constants.SERVER_PRODUCTION+"services/restIndexOwnerBy/"+ownerID+".json";
        return new ServiceRequest(Method.POST, path, listener, errorListener);
    }

    public static ServiceRequest requestListNameBy(String name, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/services/restIndexNameBy.json";
        String path = Constants.SERVER_PRODUCTION+"services/restIndexNameBy.json";
        Map<String, String> params = new HashMap<String, String>();

        params.put("name", name);
        return new ServiceRequest(Method.POST, path, params, listener, errorListener);
    }

    public static ServiceRequest requestListCategoryBy(Long categoryID, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/services/restIndexCategoryBy/"+categoryID+".json";
        String path = Constants.SERVER_PRODUCTION+"services/restIndexCategoryBy/"+categoryID+".json";
        return new ServiceRequest(Method.GET, path, listener, errorListener);
    }

    /* public static ServiceRequest requestAddService(Service service, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        String path =
    } */

    public static ServiceRequest requestOrderCall(String phone, String performerEmail, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("phone", phone);
        params.put("performerEmail", performerEmail);
        //params.put("password", user.getPassword());
        //params.put("state_id", String.valueOf(user.getState()));

        //String path = "http://s673212913.onlinehome.us/nkoserver/sandbox/app/users/restOrderCall.json";
        String path = Constants.SERVER_PRODUCTION+"users/restOrderCall.json";
        return new ServiceRequest(Method.POST, path, params, listener, errorListener);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }
}
