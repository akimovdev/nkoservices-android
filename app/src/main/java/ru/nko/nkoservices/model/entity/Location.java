package ru.nko.nkoservices.model.entity;

import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("id")
    private long serverID;

    private String name;

    private String area;

    private String country;


    public void setServerID(long serverID) {
        this.serverID = serverID;
    }

    public long getServerID() {
        return serverID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public String getCountry() {
        return country;
    }
}
