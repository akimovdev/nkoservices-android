package ru.nko.nkoservices;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;

import ru.nko.nkoservices.model.entity.User;
import ru.nko.nkoservices.ui.activities.BaseActivity;
import ru.nko.nkoservices.ui.activities.AuthActivity;
import ru.nko.nkoservices.ui.fragments.AboutFragment;
import ru.nko.nkoservices.ui.fragments.DashboardFragment;
import ru.nko.nkoservices.ui.fragments.ListServicesFragment;
import ru.nko.nkoservices.ui.fragments.ServiceFragment;
import ru.nko.nkoservices.ui.fragments.SplashFragment;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState != null)
            return;

        startSplash();
    }

    @Override
    protected void onStart() {
        super.onStart();
        buildDrawer();
    }

    private void startSplash() {
        addFragment(new SplashFragment());
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                startDashboard();
            }
        }, 2000l);
    }

    private void startDashboard() {
        replaceFragment(new DashboardFragment(), false);
        getSupportActionBar().show();
    }

    private void startAuth() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        startActivity(new Intent(this, AuthActivity.class));
    }



    private void buildDrawerHeaderView (User currentUser, View headerView) {

        ImageView ivAvatar = (ImageView) headerView.findViewById(R.id.ivAvatar);
        TextView tvLetter = (TextView) headerView.findViewById(R.id.tvLetter);
        TextView tvAuth = (TextView) headerView.findViewById(R.id.tvAuth);
        TextView tvFullName = (TextView) headerView.findViewById(R.id.tvFullName);
        TextView tvEmail = (TextView) headerView.findViewById(R.id.tvEmail);

        if (currentUser == null) {
            tvAuth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startAuth();
                }
            });
            return;
        }

        tvAuth.setVisibility(View.GONE);
        tvLetter.setVisibility(View.VISIBLE);
        ivAvatar.setVisibility(View.VISIBLE);
        tvFullName.setVisibility(View.VISIBLE);
        tvEmail.setVisibility(View.VISIBLE);

        //tvLetter.setText(currentUser.getName().charAt(0));
        tvLetter.setText(String.valueOf(currentUser.getName().charAt(0)));

        tvFullName.setText(currentUser.getName());
        tvEmail.setText(currentUser.getEmail());
    }

    private void buildDrawerMenuView (User currentUser) {

        if (currentUser == null)
            return;

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_profile).setVisible(true);
        nav_Menu.findItem(R.id.nav_logout).setVisible(true);
        nav_Menu.findItem(R.id.nav_dashboard).setVisible(true);

        if (currentUser.getState() == User.STATE_PERFORMER)
            nav_Menu.findItem(R.id.nav_own_services).setVisible(true);

    }

    private void buildDrawer() {
        User currentUser = NKOApplication.getInstance().getCurrentUser();

        buildDrawerHeaderView(currentUser, navigationView.getHeaderView(0));
        buildDrawerMenuView(currentUser);
    }


    @Override
    protected int getContainer() {
        return R.id.container;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);

        return true;
    }



    private void search(String query) {
        Fragment currentFragment = getCurrentFragment();

        if ( !(currentFragment instanceof ListServicesFragment) ) {
            currentFragment = ListServicesFragment.newInstanceSearch(query);
            replaceFragment(currentFragment, false);
            return;
        }

        ((ListServicesFragment) currentFragment).onSearchQuery(query);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
            //return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;

        if (id == R.id.nav_dashboard) {
            fragment = new DashboardFragment();
        } else if (id == R.id.nav_own_services) {
            fragment = ListServicesFragment.newInstanceOwnerBy(NKOApplication.getInstance().getCurrentUser());
        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();
        } else if (id == R.id.nav_logout) {
            NKOApplication.getInstance().removeCurrentUser();
            refresh();
        }

        if (fragment != null)
            replaceFragment(fragment, false);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void refresh() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        search(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
