package ru.nko.nkoservices.services.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.UploadFileService;

public class UploadFileReceiver extends BroadcastReceiver {

    private UploadFileListener listener;

    public UploadFileReceiver() {}

    public UploadFileReceiver(UploadFileListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        int receiveCode = intent.getIntExtra(Constants.Extras.RECEIVE, UploadFileService.RECEIVE_FAIL);

        if (receiveCode == UploadFileService.RECEIVE_PROGRESS) {
            int progress = intent.getIntExtra(Constants.Extras.PROGRESS, -1);
            listener.onProgress(progress);
        } else if (receiveCode == UploadFileService.RECEIVE_SUCCESS) {
            listener.onSucess( intent.getStringExtra(Constants.Extras.FILE_PATH) );
        } else if (receiveCode == UploadFileService.RECEIVE_FAIL) {
            listener.onError();
        }
    }

    public interface UploadFileListener {
        void onProgress(int progress);
        void onSucess(String url);
        void onError();
    }
}
