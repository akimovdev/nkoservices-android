package ru.nko.nkoservices;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import com.activeandroid.ActiveAndroid;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.squareup.leakcanary.LeakCanary;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.User;

public class NKOApplication extends Application {

    private static final String PREF_CURRENT_USER_ID = "pref_current_user";

    private volatile static NKOApplication instance;
    private User currentUser = null;
    private SharedPreferences sharedPreferences;

    public static List<Category> list;

    @Override
    public void onCreate() {
        super.onCreate();

        synchronized (NKOApplication.class) {
            if (BuildConfig.DEBUG) {
                if (instance != null)
                    throw new RuntimeException("Something strange: there is another application newInstance.");
            }
            instance = this;

            NKOApplication.class.notifyAll();
        }

        sharedPreferences = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);

        ActiveAndroid.initialize(this);
        initImageLoader(this);
        buildGson();

        /*if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this); */

    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
        Container.initialize();

    }

    private void buildGson() {
        GsonBuilder builder = new GsonBuilder();

        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        //builder.excludeFieldsWithoutExposeAnnotation();
        builder.create();

        Gson gson = builder.create();
    }

    @SuppressWarnings({"ConstantConditions", "unchecked"})
    public static NKOApplication getInstance() {
        NKOApplication application = instance;
        if (application == null) {
            synchronized (NKOApplication.class) {
                if (instance == null) {
                    if (BuildConfig.DEBUG) {
                        if (Thread.currentThread() == Looper.getMainLooper().getThread())
                            throw new UnsupportedOperationException(
                                    "Current application's newInstance has not been initialized yet (wait for onCreate, please).");
                    }

                    try {
                        do {
                            NKOApplication.class.wait();
                        } while ((application = instance) == null);
                    } catch (InterruptedException e) {
                        /* Nothing to do */
                    }
                }
            }
        }

        return application;
    }

    public User getCurrentUser() {

        if (currentUser == null && sharedPreferences.contains(PREF_CURRENT_USER_ID)) {
            Long id = sharedPreferences.getLong(PREF_CURRENT_USER_ID, -1);
            currentUser = User.load( User.class,  id);
        }

        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;

        Long id = this.currentUser.save();

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(PREF_CURRENT_USER_ID, id);
        editor.apply();
    }

    public void removeCurrentUser() {
        this.currentUser.delete();
        this.currentUser = null;

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(PREF_CURRENT_USER_ID);
        editor.apply();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
