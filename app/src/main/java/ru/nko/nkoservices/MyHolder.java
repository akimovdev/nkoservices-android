package ru.nko.nkoservices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;

public class MyHolder extends TreeNode.BaseNodeViewHolder<MyHolder.IconTreeItem> {

    public MyHolder(Context context) {
        super(context);
    }

    @Override
    public View createNodeView(TreeNode node, IconTreeItem value) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.item_tree, null, false);
        TextView tvValue = (TextView) view.findViewById(R.id.tvNode);
        tvValue.setText(value.text);
        
        return view;
    }

    public static class IconTreeItem {
        public int icon;
        public String text;
    }
}