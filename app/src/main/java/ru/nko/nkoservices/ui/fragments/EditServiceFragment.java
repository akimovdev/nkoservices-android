package ru.nko.nkoservices.ui.fragments;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.UploadFileService;
import ru.nko.nkoservices.components.BitmapUtil;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Image;
import ru.nko.nkoservices.model.entity.Location;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.model.requests.ImageRequest;
import ru.nko.nkoservices.model.requests.ServiceRequest;
import ru.nko.nkoservices.services.receivers.UploadFileReceiver;
import ru.nko.nkoservices.ui.activities.PlacesActivity;
import ru.nko.nkoservices.ui.activities.ServiceActivity;
import ru.nko.nkoservices.ui.activities.SubcategoriesActivity;

import static android.app.Activity.RESULT_OK;

public class EditServiceFragment extends BaseFragment<ServiceActivity> implements AdapterView.OnItemSelectedListener,
                                                        UploadFileReceiver.UploadFileListener,
                                                        RadioGroup.OnCheckedChangeListener {

    private static final String TAG = "EDIT_SERVICE_FRAGMENT";

    private static final String ARG_MODE = "mode";

    private static final int MODE_ADD = 0;
    private static final int MODE_EDIT = 1;
    private static final int MODE_ERROR = -1;

    private static final int REQUEST_OPEN_GALLERY = 1;
    private static final int REQUEST_OPEN_CATEGORIES = 2;
    private static final int REQUEST_OPEN_LOCATIONS = 3;

    private BitmapUtil bitmapUtil;
    private Uri imageUri;
    private Category selectedCategory;
    private Location selectedLocation;
    private UploadFileReceiver receiver;
    private Service createdService;
    private int selectedTypeID = -1;
    private int mode;

    private EditText etName;
    private EditText etOverview;
    private EditText etCost;
    private EditText etCategory;
    private EditText etLocation;
    private TextInputLayout wrapCost;
    private RadioButton radioFree;
    private RadioButton radioPay;
    Button buttonDone;
    private RequestQueue queue;

    public EditServiceFragment() {
        // Required empty public constructor
    }

    public static EditServiceFragment newInstanceAddService() {
        EditServiceFragment fragment = new EditServiceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, MODE_ADD);
        fragment.setArguments(args);
        return fragment;
    }

    public static EditServiceFragment newInstanceEditService(Service service) {
        EditServiceFragment fragment = new EditServiceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, MODE_EDIT);
        fragment.setService(service);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null) {
            throw new Error("Arguments has been lost");
        }

        mode = getArguments().getInt(ARG_MODE, MODE_ERROR);

        selectedTypeID = Service.TYPE_FREE;
        bitmapUtil = new BitmapUtil();

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        getHostActivity().getSupportActionBar().setTitle(R.string.title_new_service);

        buttonDone = (Button) view.findViewById(R.id.buttonDone);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.rgPay);
        radioGroup.setOnCheckedChangeListener(this);

        queue = Volley.newRequestQueue(getContext());

        wrapCost = (TextInputLayout) view.findViewById(R.id.wrapCost);

        ImageView ivImage = (ImageView) view.findViewById(R.id.ivImage);
        RelativeLayout rlGallery = (RelativeLayout) view.findViewById(R.id.rLGallery);
        etName = (EditText) view.findViewById(R.id.etName);
        etOverview = (EditText) view.findViewById(R.id.etOverview);
        etCost = (EditText) view.findViewById(R.id.etCost);
        etCategory = (EditText) view.findViewById(R.id.etCategory);
        etLocation = (EditText) view.findViewById(R.id.etLocation);
        etLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLocations();
            }
        });
        etCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCategories();
            }
        });

        radioFree = (RadioButton) view.findViewById(R.id.radioFree);
        radioPay = (RadioButton) view.findViewById(R.id.radioPay);

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getHostActivity().showProgresDialog(getString(R.string.alert_request_in_progress));
                actionDone();

                //if (imageUri == null)
                    //actionDone();
                //else
                    //startService();
            }
        });

        receiver = new UploadFileReceiver(this);

        if (mode == MODE_EDIT) {
            rlGallery.setVisibility(View.GONE);
            buildEditView();
        }
    }

    private void buildEditView() {
        etName.setText(createdService.getName());
        etOverview.setText(createdService.getOverview());
        etCategory.setText(createdService.getCategory().getName());
        etLocation.setText(createdService.getLocation().getName());
        etLocation.setText(createdService.getLocation().getName());
        buttonDone.setText(getString(R.string.action_edit));

        if (createdService.getTypeID() == Service.TYPE_PAY) {
            radioFree.setChecked(false);
            radioPay.setChecked(true);
            //etCost.setVisibility(View.VISIBLE);
            etCost.setText(String.valueOf(createdService.getCost()));
        } else {
            radioFree.setChecked(true);
            radioPay.setChecked(false);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(receiver, new IntentFilter(UploadFileService.ON_PROGRESS_ACTION));
    }

    @Override
    public void onStop() {
        super.onStop();
        getHostActivity().dismissProgressDialog();
        getActivity().unregisterReceiver(receiver);

        if (queue != null)
            queue.cancelAll(TAG);
    }

    private void actionDone() {

        /*Service service = new Service();

        service.setName(etName.getText().toString());
        service.setOverview(etOverview.getText().toString());
        service.setCategory(selectedCategory);
        service.setLocation(selectedLocation);
        service.setPerformer(NKOApplication.getInstance().getCurrentUser());
        service.setTypeID(selectedTypeID);

        if (selectedTypeID == Service.TYPE_PAY)
            service.setCost(Integer.valueOf(etCost.getText().toString()));*/

        if (mode == MODE_ADD)
            requestAddService();
        else
            requestEditService();
    }

    private void openLocations() {
        Intent intent = new Intent(getContext(), PlacesActivity.class);
        startActivityForResult(intent, REQUEST_OPEN_LOCATIONS);
    }

    private void openCategories() {

        Category category = new Category();
        category.setName(getString(R.string.select_category));
        category.setChildren(NKOApplication.list);

        Intent intent = new Intent(getContext(), SubcategoriesActivity.class);
        String key = Container.getInstance().push(category);
        intent.putExtra(Constants.Extras.CONTAINER_CATEGORIES, key);
        intent.putExtra(Constants.Extras.MODE, Constants.Mode.SELECT_CATEGORY);
        startActivityForResult(intent, REQUEST_OPEN_CATEGORIES);
    }

    private void openGallery() {

        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(intent, REQUEST_OPEN_GALLERY);
    }

    private void startService(long createdServiceID) {
        Intent intent = new Intent(getContext(), UploadFileService.class);
        intent.putExtra(Constants.Extras.IMAGE_URI, imageUri);
        intent.putExtra(Constants.Extras.SERVICE_ID, createdServiceID);

        getActivity().startService(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void requestAddService() {

        Service service = new Service();

        service.setName(etName.getText().toString());
        service.setOverview(etOverview.getText().toString());
        service.setCategory(selectedCategory);
        service.setLocation(selectedLocation);
        service.setPerformer(NKOApplication.getInstance().getCurrentUser());
        service.setTypeID(selectedTypeID);

        if (selectedTypeID == Service.TYPE_PAY)
            service.setCost(Integer.valueOf(etCost.getText().toString()));

        ServiceRequest serviceRequest = ServiceRequest.requestAddService(service, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    parseAddResult(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    getHostActivity().dismissProgressDialog();
                    Toast.makeText(getContext(), getString(R.string.alert_fail_handle_result), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                error.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);
    }

    private void requestEditService() {

        createdService.setName(etName.getText().toString());
        createdService.setOverview(etOverview.getText().toString());

        if (selectedCategory != null)
            createdService.setCategory(selectedCategory);

        if (selectedLocation != null)
            createdService.setLocation(selectedLocation);

        createdService.setTypeID(selectedTypeID);

        if (selectedTypeID == Service.TYPE_PAY)
            createdService.setCost(Integer.valueOf(etCost.getText().toString()));


        ServiceRequest serviceRequest = ServiceRequest.requestEditService(createdService, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    parseAddResult(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    getHostActivity().dismissProgressDialog();
                    Toast.makeText(getContext(), getString(R.string.alert_fail_handle_result), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                error.printStackTrace();
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);
    }

    private void successfulFinish(Service service) {

        Intent intent = new Intent();
        String key = Container.getInstance().push(service);
        intent.putExtra(Constants.Extras.CONTAINER_SERVICE, key);

        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }

    private void parseAddResult(String response/*, Service service*/) throws JSONException {



        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();
        JSONObject jsonResponse = new JSONObject(response);

        if ( jsonResponse.has(Constants.RESULT) ) {
            jsonResponse = jsonResponse.getJSONObject(Constants.RESULT);
            if ( jsonResponse.getInt(Constants.STATUS) == Constants.STATUS_SUCCESSFUL ) {
                //Toast.makeText(getContext(), "Successful", Toast.LENGTH_LONG).show();
                //getActivity().finish();

                if (mode == MODE_ADD)
                    createdService = gson.fromJson(jsonResponse.getString(Constants.SERVICE), Service.class);

                if (imageUri == null)
                    successfulFinish(createdService);
                else
                    startService(createdService.getServerID());


            } else {
                Toast.makeText(getContext(), jsonResponse.getString(Constants.MESSAGE), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void onSuccessfulActivityResult(int requestCode, Intent data) {

        if ( requestCode == REQUEST_OPEN_GALLERY ) {
            imageUri = data.getData();
            ((ImageView) getView().findViewById(R.id.ivImage))
                    //.setImageBitmap(bitmapUtil.decodeSampledBitmapFromResource(getContext(), imageUri, 720, 480));
                    .setImageBitmap(bitmapUtil.decodeSampledBitmapFromResourceNEW2(getContext(), imageUri, 720, 480));

        } else if ( requestCode == REQUEST_OPEN_CATEGORIES ) {
            selectedCategory = new Category();
            selectedCategory.setServerId(data.getLongExtra(Constants.Extras.CATEGORY_ID, -1));
            selectedCategory.setName(data.getStringExtra(Constants.Extras.CATEGORY_NAME));
            etCategory.setText(selectedCategory.getName());

        } else if ( requestCode == REQUEST_OPEN_LOCATIONS ) {
            selectedLocation = new Location();
            selectedLocation.setServerID(data.getLongExtra(Constants.Extras.LOCATION_ID, -1));
            selectedLocation.setName(data.getStringExtra(Constants.Extras.LOCATION_NAME));
            selectedLocation.setArea(data.getStringExtra(Constants.Extras.LOCATION_AREA));
            etLocation.setText(selectedLocation.getName());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
            onSuccessfulActivityResult(requestCode, data);
    }

    @Override
    public void onProgress(int progress) {
        //tvProgress.setText(String.valueOf(progress));
        //progressBar.setProgress(progress);
    }

    @Override
    public void onSucess(String url) {
        //Toast.makeText(getContext(), getString(R.string.alert_success_loading), Toast.LENGTH_SHORT).show();

        createdService.addImageUrl(url);

        Image image = new Image();
        image.setUrl(url);
        image.setServiceID(createdService.getServerID());

        ImageRequest imageRequest = ImageRequest.requestAddImage(image, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                successfulFinish(createdService);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
            }
        });

        imageRequest.setTag(TAG);
        queue.add(imageRequest);
    }

    @Override
    public void onError() {
        Toast.makeText(getContext(), getString(R.string.alert_fail_loading), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

        switch(checkedId) {

            case R.id.radioFree:
                wrapCost.setVisibility(View.INVISIBLE);
                selectedTypeID = Service.TYPE_FREE;
                break;

            case R.id.radioPay:
                wrapCost.setVisibility(View.VISIBLE);
                selectedTypeID = Service.TYPE_PAY;
                break;
        }
    }

    private void setService(Service service) {
        this.createdService = service;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;

        }

        return super.onOptionsItemSelected(item);
    }
}
