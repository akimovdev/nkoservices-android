package ru.nko.nkoservices.ui.fragments;

import android.support.v4.app.Fragment;
import android.util.Log;

import ru.nko.nkoservices.ui.activities.BaseActivity;


public class BaseFragment<ActivityClass extends BaseActivity> extends Fragment {

    private final static String TAG = "BaseFragment";

    //private DatabaseReference connectedRef;
    //private ValueEventListener connectListener;

    public ActivityClass getHostActivity() {

        return (ActivityClass) getActivity();
    }

    protected void onConnected() {
        /* Nothing to do */
        Log.d(TAG, "onConnected()");
    }

    public void onDisconnected() {
        /* Nothing to do */
        Log.d(TAG, "onDisconnected()");
    }



}

