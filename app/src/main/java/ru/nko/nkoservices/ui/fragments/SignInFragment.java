package ru.nko.nkoservices.ui.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Modifier;
import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.User;
import ru.nko.nkoservices.model.requests.UserRequest;
import ru.nko.nkoservices.ui.activities.AuthActivity;

import static android.app.Activity.RESULT_OK;

public class SignInFragment extends BaseFragment<AuthActivity> {

    private static final String TAG = "SIGN_IN_FRAGMENT";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Dialog dialog;

    private TextInputLayout wrapEmail;
    private TextInputLayout wrapPassword;

    private EditText etEmail;
    private EditText etPassword;
    private Button buttonSignIn;
    private TextView tvRegister;
    private TextView tvRegisterPerformer;
    private TextView tvForgot;

    View dialogView;

    private RequestQueue queue;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }

    @Override
    public void onStop() {
        super.onStop();
        getHostActivity().dismissProgressDialog();

        if (dialog != null)
            dialog.dismiss();

        if (queue != null)
            queue.cancelAll(TAG);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        queue = Volley.newRequestQueue(getContext());

        wrapEmail = (TextInputLayout) view.findViewById(R.id.wrapEmail);
        wrapPassword = (TextInputLayout) view.findViewById(R.id.wrapPassword);

        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        buttonSignIn = (Button) view.findViewById(R.id.buttonSignIn);
        tvRegister = (TextView) view.findViewById(R.id.tvRegister);
        tvRegisterPerformer = (TextView) view.findViewById(R.id.tvRegisterPerformer);
        tvForgot = (TextView) view.findViewById(R.id.tvForgot);

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHostActivity().replaceFragment(SignUpFragment.newInstanceUser(), true);
            }
        });

        tvRegisterPerformer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHostActivity().replaceFragment(SignUpFragment.newInstancePerformer(), true);
            }
        });

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!NKOApplication.getInstance().isNetworkAvailable()) {
                    Toast.makeText(getContext(), getString(R.string.alert_lost_internet_connection), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!validateInput())
                    return;

                getHostActivity().showProgresDialog(getString(R.string.alert_data_process));
                auth(etEmail.getText().toString(), etPassword.getText().toString());
            }
        });

        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotDialog(etEmail.getText().toString());
            }
        });
    }

    private void requestForgot(String email) {
        StringRequest recoverPasswordRequest = UserRequest.requestRecoverPassword(email, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseForgotResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_LONG).show();
            }
        });

        recoverPasswordRequest.setTag(TAG);
        queue.add(recoverPasswordRequest);
    }

    private void auth(final String email, final String password) {


        StringRequest stringRequest = UserRequest.login(email, password, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseAuthResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_LONG).show();
            }
        });

        stringRequest.setTag(TAG);
        queue.add(stringRequest);
    }

    private void successfulFinish() {
        Intent intent = new Intent();
        intent.putExtra(Constants.Extras.AUTH_RESULT, true);
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }

    private void parseForgotResult(String response) {
        getHostActivity().dismissProgressDialog();
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        try {
            JSONObject jsonResponse = new JSONObject(response);

            if (jsonResponse.has(Constants.STATUS)) {

                int status = jsonResponse.getInt(Constants.STATUS);

                if ( status == Constants.STATUS_SUCCESSFUL) {

                    Toast.makeText(getContext(), getString(R.string.alert_password_recovered), Toast.LENGTH_LONG).show();

                } else if (status == Constants.STATUS_FAIL) {
                    Toast.makeText(getContext(), jsonResponse.getString(Constants.MESSAGE), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
        }

    }

    private void parseAuthResult(String response) {

        //Gson gson = new Gson();
        getHostActivity().dismissProgressDialog();
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        try {
            JSONObject jsonResponse = new JSONObject(response);

            if (jsonResponse.has(Constants.STATUS)) {
                if (jsonResponse.getInt(Constants.STATUS) == Constants.STATUS_SUCCESSFUL) {

                    User currentUser = gson.fromJson(jsonResponse.getString(Constants.USER), User.class);
                    NKOApplication.getInstance().setCurrentUser(currentUser);
                    successfulFinish();
                    //getActivity().finish();
                } else {
                    //Toast.makeText(getContext(), jsonResponse.getString(Constants.MESSAGE), Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getContext(), jsonResponse.getString(Constants.MESSAGE), Toast.LENGTH_SHORT).show();
                    Toast.makeText(getContext(), getString(R.string.alert_incorrect_login_or_pass), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
        }

        //Toast.makeText(getContext(), "Successful", Toast.LENGTH_SHORT).show();
    }

    private void resetError() {
        wrapEmail.setErrorEnabled(false);;
        wrapPassword.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(etEmail.getText())) {
            wrapEmail.setErrorEnabled(true);
            wrapEmail.setError(getString(R.string.alert_emprty_field));
            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches()) {
            wrapEmail.setErrorEnabled(true);
            wrapEmail.setError(getString(R.string.alert_unappropriate_format));
            return false;

        } else if (TextUtils.isEmpty(etPassword.getText())) {
            wrapPassword.setErrorEnabled(true);
            wrapPassword.setError(getString(R.string.alert_emprty_field));
            return false;
        }

        return true;
    }



    private void showForgotDialog(String email) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_forgot, null);
        final EditText etEmail = (EditText) dialogView.findViewById(R.id.etEmail);
        etEmail.setText(email);

        builder.setTitle(getString(R.string.title_recover_password));
        builder.setView(dialogView)
                .setPositiveButton(R.string.action_send, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        getHostActivity().showProgresDialog(getString(R.string.alert_data_process));
                        requestForgot(etEmail.getText().toString());

                        if (dialog != null) {
                            dialog.cancel();
                        }
                    }
                })
                .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                    }
                });
        dialog = builder.create();
        dialog.show();

    }
}
