package ru.nko.nkoservices.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import ru.nko.nkoservices.R;
import ru.nko.nkoservices.ui.fragments.LegalFragment;

public class LegalActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.title_information));
        }

        if (savedInstanceState != null)
            return;

        addFragment(new LegalFragment());
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }
}
