package ru.nko.nkoservices.ui.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.ui.fragments.ServiceFragment;

public class DetailServiceActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_overlay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null)
            return;

        String categoryKey = getIntent().getStringExtra(Constants.Extras.CONTAINER_MODEL);

        if (categoryKey == null)
            throw new Error("StartMode parameter has been lost. Please use factory method for start activity !!!");

        //Service service = (Service) Container.getInstance().pop(categoryKey);
        addFragment( ServiceFragment.newInstance(categoryKey) );
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.service_detail, menu);
        return true;
    }


    public static void startDetailServiceActivity(Context context, Service service) {
        Intent intent = new Intent(context, DetailServiceActivity.class);

        intent.putExtra( Constants.Extras.CONTAINER_MODEL, Container.getInstance().push(service) );
        context.startActivity(intent);
    }
}
