package ru.nko.nkoservices.ui.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.Locale;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.MainActivity;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.User;
import ru.nko.nkoservices.model.requests.UserRequest;
import ru.nko.nkoservices.ui.activities.AuthActivity;
import ru.nko.nkoservices.ui.activities.LegalActivity;

public class SignUpFragment extends BaseFragment<AuthActivity> {

    private static final String TAG = "SIGN_UP_FRAGMENT";

    private static final int START_USER = 0;
    private static final int START_PERFORMER = 1;

    private static final String ARG_START_TYPE = "start_type";

    private int startType;

    private TextInputLayout wrapEmail;
    private TextInputLayout wrapName;
    private TextInputLayout wrapPhone;
    private TextInputLayout wrapPassword;
    private TextInputLayout wrapAccreditation;
    private TextInputLayout wrapExperience;
    private TextInputLayout wrapLeader;

    private CheckBox checkBox;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etName;
    private EditText etPhone;
    private EditText etLeader;
    private EditText etExperience;
    private EditText etAccreditation;
    private Button buttonDone;
    private boolean isPhoneFocusable = false;

    private RequestQueue queue;

    public SignUpFragment() {
        // Required empty public constructor
    }


    public static SignUpFragment newInstanceUser() {

        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_USER);
        fragment.setArguments(args);
        return fragment;
    }

    public static SignUpFragment newInstancePerformer() {

        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_START_TYPE, START_PERFORMER);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() == null)
            throw new Error("ServiceFragment can not be start without arguments. Use please factory method for start ServiceFragment !!!");

        startType = getArguments().getInt(ARG_START_TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        queue = Volley.newRequestQueue(getContext());

        TextView tvLink = (TextView) view.findViewById(R.id.tvLink);

        wrapEmail = (TextInputLayout) view.findViewById(R.id.wrapEmail);
        wrapName = (TextInputLayout) view.findViewById(R.id.wrapName);
        wrapPhone = (TextInputLayout) view.findViewById(R.id.wrapPhone);
        wrapPassword = (TextInputLayout) view.findViewById(R.id.wrapPassword);

        checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etName = (EditText) view.findViewById(R.id.etName);
        etPhone = (EditText) view.findViewById(R.id.etPhone);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        buttonDone = (Button) view.findViewById(R.id.buttonDone);

        etPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                isPhoneFocusable = hasFocus;

                if (hasFocus && etPhone.getText().toString().isEmpty()) {
                    etPhone.setText("+7");
                    etPhone.setSelection(etPhone.getText().length());
                } else if (etPhone.getText().length() == 2) {
                    etPhone.getText().clear();
                }
            }
        });

        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Toast.makeText(getContext(), "Before", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Toast.makeText(getContext(), "onTextChanged", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(getContext(), "afterTextChanged", Toast.LENGTH_SHORT).show();
                if(!s.toString().contains("+7") && isPhoneFocusable) {
                    etPhone.setText("+7");
                    Selection.setSelection(etPhone.getText(), etPhone.getText().length());
                }
            }
        });


        if (startType == START_PERFORMER)
            buildPerformerView(view);

        buttonDone.setOnClickListener( startType == START_USER ? actionRegister() : actionRegisterPerformer() );

        tvLink.setMovementMethod(LinkMovementMethod.getInstance());
        tvLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LegalActivity.class);
                getContext().startActivity(intent);
            }
        });




        /*Spanned legacy;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            legacy = Html.fromHtml(getString(R.string.legacy_link), Html.FROM_HTML_MODE_LEGACY);
        } else {
            legacy = Html.fromHtml(getString(R.string.legacy_link));
        }

        tvLink.setText(legacy); */

    }

    @Override
    public void onStop() {
        super.onStop();
        getHostActivity().dismissProgressDialog();

        if (queue != null)
            queue.cancelAll(TAG);
    }



    private void buildPerformerView(View view) {

        wrapAccreditation = (TextInputLayout) view.findViewById(R.id.wrapAccreditation);
        wrapExperience = (TextInputLayout) view.findViewById(R.id.wrapExperience);
        wrapLeader = (TextInputLayout) view.findViewById(R.id.wrapLeader);

        etLeader = (EditText) view.findViewById(R.id.etLeader);
        etAccreditation = (EditText) view.findViewById(R.id.etAccreditation);
        etExperience = (EditText) view.findViewById(R.id.etExperience);

        wrapLeader.setVisibility(View.VISIBLE);
        wrapAccreditation.setVisibility(View.VISIBLE);
        wrapExperience.setVisibility(View.VISIBLE);
    }


    private void parseOrederPerformerResult(String response) throws JSONException {

        getHostActivity().dismissProgressDialog();

        //Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();
        JSONObject jsonResponse = new JSONObject(response);

        if (jsonResponse.has(Constants.RESULT)) {
            jsonResponse = jsonResponse.getJSONObject(Constants.RESULT);
            if ( jsonResponse.getInt(Constants.STATUS) == Constants.STATUS_SUCCESSFUL ) {

                Toast.makeText(NKOApplication.getInstance(), getString(R.string.alert_order_was_accepted), Toast.LENGTH_SHORT).show();
                getActivity().finish();

            } else {
                int messageResource = UserRequest.getMessageResource(jsonResponse.getInt(Constants.ERROR));
                Toast.makeText(getContext(), getString(messageResource), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void parseRegisterResult(String response) throws JSONException {

        getHostActivity().dismissProgressDialog();

        //Gson gson = new Gson();
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();
        JSONObject jsonResponse = new JSONObject(response);

        if (jsonResponse.has(Constants.RESULT)) {
            jsonResponse = jsonResponse.getJSONObject(Constants.RESULT);
            if ( jsonResponse.getInt(Constants.STATUS) == Constants.STATUS_SUCCESSFUL ) {

                Toast.makeText(NKOApplication.getInstance(), getString(R.string.alert_thanks_for_registration), Toast.LENGTH_SHORT).show();

                User currentUser = gson.fromJson(jsonResponse.getString(Constants.USER), User.class);
                NKOApplication.getInstance().setCurrentUser(currentUser);
                getActivity().finish();

            } else {
                int messageResource = UserRequest.getMessageResource(jsonResponse.getInt(Constants.ERROR));
                Toast.makeText(getContext(), getString(messageResource), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void requestRegister(User user) {

        StringRequest request = UserRequest.register(user, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    parseRegisterResult(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    getHostActivity().dismissProgressDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        request.setTag(TAG);
        queue.add(request);
    }

    private void requestRegisterPerformer(User user) {
        StringRequest request = UserRequest.requestOrderPerfo(user, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    parseOrederPerformerResult(response);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    getHostActivity().dismissProgressDialog();
                    Toast.makeText(getContext(), getString(R.string.alert_progress_error), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        request.setTag(TAG);
        queue.add(request);
    }

    private View.OnClickListener actionRegister() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!NKOApplication.getInstance().isNetworkAvailable()) {
                    Toast.makeText(getContext(), getString(R.string.alert_lost_internet_connection), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!validateInput()) return;

                User user = User.createUser();
                user.setEmail( etEmail.getText().toString() );
                user.setName( etName.getText().toString() );
                user.setPassword( etPassword.getText().toString() );
                user.setPhone( etPhone.getText().toString() );


                getHostActivity().showProgresDialog(getString(R.string.alert_data_process));
                requestRegister(user);
            }
        };
    }

    private View.OnClickListener actionRegisterPerformer() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!NKOApplication.getInstance().isNetworkAvailable()) {
                    Toast.makeText(getContext(), getString(R.string.alert_lost_internet_connection), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!inputValidPerformer()) return;

                User user = User.createPerformer();
                user.setEmail( etEmail.getText().toString() );
                user.setName( etName.getText().toString() );
                user.setPassword( etPassword.getText().toString() );
                user.setPhone( etPhone.getText().toString() );
                user.setLeader( etLeader.getText().toString() );
                user.setExperiance( etExperience.getText().toString() );
                user.setAccreditation( etAccreditation.getText().toString() );

                getHostActivity().showProgresDialog(getString(R.string.alert_data_process));
                requestRegisterPerformer(user);
            }
        };
    }

    private void resetError() {
        wrapEmail.setErrorEnabled(false);
        wrapName.setErrorEnabled(false);
        wrapPhone.setErrorEnabled(false);
        wrapPassword.setErrorEnabled(false);
    }

    private boolean validateInput() {

        resetError();

        if (TextUtils.isEmpty(etEmail.getText())) {
            wrapEmail.setErrorEnabled(true);
            wrapEmail.setError(getString(R.string.alert_emprty_field));
            return false;

        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches()) {
            wrapEmail.setErrorEnabled(true);
            wrapEmail.setError(getString(R.string.alert_unappropriate_format));
            return false;

        } if (TextUtils.isEmpty(etName.getText())) {
            wrapName.setErrorEnabled(true);
            wrapName.setError(getString(R.string.alert_emprty_field));
            return false;

        } if (TextUtils.isEmpty(etPhone.getText())) {
            wrapPhone.setErrorEnabled(true);
            wrapPhone.setError(getString(R.string.alert_emprty_field));
            return false;

        } else if (!Patterns.PHONE.matcher(etPhone.getText()).matches()) {
            wrapPhone.setErrorEnabled(true);
            wrapPhone.setError(getString(R.string.alert_unappropriate_format));
            return false;

        } else if (TextUtils.isEmpty(etPassword.getText())) {
            wrapPassword.setErrorEnabled(true);
            wrapPassword.setError(getString(R.string.alert_emprty_field));
            return false;
        } else if (!checkBox.isChecked()) {
            Toast.makeText(getContext(), getString(R.string.alert_legacy), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private boolean inputValidPerformer() {

        if (!validateInput()) {
            return false;
        } else if (TextUtils.isEmpty(etLeader.getText())) {
            wrapLeader.setError(getString(R.string.alert_emprty_field));
            return false;
        } else if (TextUtils.isEmpty(etAccreditation.getText())) {
            wrapAccreditation.setError(getString(R.string.alert_emprty_field));
            return false;
        } else if (TextUtils.isEmpty(etExperience.getText())) {
            wrapExperience.setError(getString(R.string.alert_emprty_field));
            return false;
        }

        return true;
    }

}
