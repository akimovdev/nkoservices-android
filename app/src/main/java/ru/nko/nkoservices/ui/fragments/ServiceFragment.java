package ru.nko.nkoservices.ui.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.UploadFileService;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Image;
import ru.nko.nkoservices.model.entity.Location;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.model.entity.User;
import ru.nko.nkoservices.model.requests.GeocodingRequest;
import ru.nko.nkoservices.model.requests.ImageRequest;
import ru.nko.nkoservices.model.requests.ServiceRequest;
import ru.nko.nkoservices.model.requests.UserRequest;
import ru.nko.nkoservices.services.receivers.UploadFileReceiver;
import ru.nko.nkoservices.ui.activities.DetailServiceActivity;
import ru.nko.nkoservices.ui.activities.ServiceActivity;
import ru.nko.nkoservices.ui.adapters.GalleryAdapter;

import static android.app.Activity.RESULT_OK;

public class ServiceFragment extends BaseFragment<DetailServiceActivity> implements OnMapReadyCallback,
                                                                    UploadFileReceiver.UploadFileListener {

    private static final int REQUEST_OPEN_GALLERY = 1;
    private final static int REQUEST_EDIT_SERVICE = 2;

    private static final String ARG_CATEGORY_KEY = "category_key";
    private static final float ZOOM_MAP = 12f;
    private static final String TAG = "SERVICE_FRAGMENT";
    private static final int PERMISSIONS_PHONE_REQUEST = 1;

    private RecyclerViewPager rvGallery;
    private GoogleMap googleMap;
    private MapView mapView;
    private String calledNumber;
    private UploadFileReceiver receiver;

    private RequestQueue queue;
    private Service service;

    public ServiceFragment() {
        // Required empty public constructor
    }

    public static ServiceFragment newInstance(String categoryKey) {
        ServiceFragment fragment = new ServiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY_KEY, categoryKey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() == null) {
            throw new Error("ServiceFragment can not be start without arguments. Use please factory method for start ServiceFragment !!!");
        }

        String categoryKey = getArguments().getString(ARG_CATEGORY_KEY, null);

        if ( categoryKey == null )
            throw new Error("Category Key parameter has been lost");

        this.service = (Service) Container.getInstance().pop(categoryKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_service, container, false);
    }


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        TextView tvHeadline = (TextView) view.findViewById(R.id.tvHeadline);
        TextView tvLocation = (TextView) view.findViewById(R.id.tvLocation);
        TextView tvCost = (TextView) view.findViewById(R.id.tvCost);

        rvGallery = (RecyclerViewPager) view.findViewById(R.id.rvGallery);
        rvGallery.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        if (service.getImages() != null)
            rvGallery.setAdapter(new GalleryAdapter(service.getImages()));

        tvHeadline.setText(service.getName());
        tvLocation.setText(service.getLocation().getArea()+" "+service.getLocation().getName());

        if (service.getTypeID() == Service.TYPE_FREE)
            tvCost.setText(R.string.free);
        else
            tvCost.setText(service.getCost()+Constants.SYMBOL_RUB);

        queue = Volley.newRequestQueue(getContext());

        ServiceRequest serviceRequest = ServiceRequest.requestInstanceIdBy(service.getServerID(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    parseRequestResult(response, view);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        receiver = new UploadFileReceiver(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
        getActivity().registerReceiver(receiver, new IntentFilter(UploadFileService.ON_PROGRESS_ACTION));
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
        getActivity().unregisterReceiver(receiver);

        if (queue != null) {
            queue.cancelAll(TAG);
        }

        getHostActivity().dismissProgressDialog();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void startService(long createdServiceID, Uri imageUri) {
        Intent intent = new Intent(getContext(), UploadFileService.class);
        intent.putExtra(Constants.Extras.IMAGE_URI, imageUri);
        intent.putExtra(Constants.Extras.SERVICE_ID, createdServiceID);

        getActivity().startService(intent);
    }

    private void openGallery() {

        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(intent, REQUEST_OPEN_GALLERY);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        User currentUser = NKOApplication.getInstance().getCurrentUser();
        User performer = service.getPerformer();

        if ( performer != null && (currentUser.getServerID() == performer.getServerID()) )
            menu.findItem(R.id.action_add).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;

            case R.id.action_add:
                openGallery();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void parseRequestResult(String response, View view) throws JSONException {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();


        JSONObject jsonResponse = new JSONObject(response);

        if (jsonResponse.has(Constants.SERVICE)) {
            //jsonResponse = jsonResponse.getJSONObject(Constants.SERVICE);
            Service service = gson.fromJson(jsonResponse.getString(Constants.SERVICE), Service.class);
            //service.getCity();
            buildView(view, service);
        } else {
            Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void requestCallNumberNotificatio(User user) {

        StringRequest request = UserRequest.requestCallNotification(user, service, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        request.setTag(TAG);
        queue.add(request);
    }

    private void callNumber(String phone) {

        User currentUser = NKOApplication.getInstance().getCurrentUser();

        if (currentUser != null) {
            requestCallNumberNotificatio(currentUser);
        }

        StringBuilder stringBuilder = new StringBuilder("tel:");
        stringBuilder.append(phone);

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(stringBuilder.toString()));
        startActivity(callIntent);
    }

    private void buildView(View view, final Service service) {

        this.service = service;

        getHostActivity().invalidateOptionsMenu();

        RelativeLayout rvPhoneArea = (RelativeLayout) view.findViewById(R.id.rvPhoneArea);
        ImageView ivCall = (ImageView) view.findViewById(R.id.ivCall);
        TextView tvPhone = (TextView) view.findViewById(R.id.tvPhone);
        TextView tvLocation = (TextView) view.findViewById(R.id.tvLocation);
        TextView tvCost = (TextView) view.findViewById(R.id.tvCost);

        TextView tvHeadline = (TextView) view.findViewById(R.id.tvHeadline);
        TextView tvOverview = (TextView) view.findViewById(R.id.tvOverview);
        Button buttonOrderCall = (Button) view.findViewById(R.id.buttonOrderCall);

        tvHeadline.setText(service.getName());
        tvPhone.setText(service.getPerformer().getPhone());
        tvOverview.setText(service.getOverview());
        tvLocation.setText(service.getLocation().getArea()+" "+service.getLocation().getName());

        if (service.getTypeID() == Service.TYPE_FREE)
            tvCost.setText(R.string.free);
        else
            tvCost.setText(service.getCost()+Constants.SYMBOL_RUB);

        ivCall.setVisibility(View.VISIBLE);

        rvPhoneArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CALL_PHONE);

                if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                    callNumber(service.getPerformer().getPhone());
                } else {
                    calledNumber = service.getPerformer().getPhone();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.CALL_PHONE)) {
                        requestPermissions( new String[]{Manifest.permission.CALL_PHONE},
                                PERMISSIONS_PHONE_REQUEST);
                    } else {
                        // No explanation needed, we can request the permission.
                        requestPermissions( new String[]{Manifest.permission.CALL_PHONE},
                                PERMISSIONS_PHONE_REQUEST);
                    }
                }
            }
        });

        if (service.getPerformer().getServerID() == NKOApplication.getInstance().getCurrentUser().getServerID()) {
            buttonOrderCall.setText(getString(R.string.action_edit));
            buttonOrderCall.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryGreen));
            buttonOrderCall.setOnClickListener(actionEdit(service));
        } else {
            buttonOrderCall.setOnClickListener(actionOrderCall(service));
        }
    }

    private View.OnClickListener actionOrderCall(final Service service) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = NKOApplication.getInstance().getCurrentUser().getPhone();
                requestOrderCall(phone, service.getPerformer().getEmail());
            }
        };
    }



    private View.OnClickListener actionEdit(final Service service) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String phone = NKOApplication.getInstance().getCurrentUser().getPhone();
                //requestOrderCall(phone, service.getPerformer().getEmail());
                //Toast.makeText(getContext(), "In development", Toast.LENGTH_SHORT).show();
                //getHostActivity().replaceFragment(EditServiceFragment.newInstanceEditService(service), true);

                startActivityForResult(ServiceActivity.intentForEditActivity(getContext(), service), REQUEST_EDIT_SERVICE);
            }
        };
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSIONS_PHONE_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    callNumber(calledNumber);
                }
                return;
            }
        }
    }


    private void showLocation (double lat, double lng) {

        LatLng latLng= new LatLng(lat, lng);
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(ZOOM_MAP));
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    private void parseLocationResult(String response) throws JSONException {
        JSONObject jsonResponse = new JSONObject(response);

        String status = jsonResponse.getString(Constants.STATUS);

        if (status == null || !status.equals(GeocodingRequest.RESPONSE_OK)) {
            Toast.makeText(getContext(), getString(R.string.alert_fail_load_location), Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject geometryObject = jsonResponse.getJSONArray(Constants.RESULTS)
                .getJSONObject(0)
                .getJSONObject(Constants.GEOMETRY);

        if (geometryObject == null) {
            Toast.makeText(getContext(), getString(R.string.alert_fail_load_location), Toast.LENGTH_SHORT).show();
            return;
        }

        double lat = geometryObject.getJSONObject(Constants.LOCATION).getDouble(Constants.LAT);
        double lng = geometryObject.getJSONObject(Constants.LOCATION).getDouble(Constants.LNG);

        showLocation(lat, lng);
    }

    private void requestLocation() {

        String address = service.getLocation().getName() + ","
                + service.getLocation().getArea() + ","
                + service.getLocation().getCountry();

        GeocodingRequest geocodingRequest = GeocodingRequest.requestLocationAddressBy(address, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains(Constants.RESULTS)) {

                    try {
                        parseLocationResult(response);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                } else {
                    Toast.makeText(getContext(), getString(R.string.alert_fail_load_location), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.alert_fail_load_location), Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });

        geocodingRequest.setTag(TAG);
        queue.add(geocodingRequest);
    }

    private void requestOrderCall(String phone, String performerEmail) {

        getHostActivity().showProgresDialog(getString(R.string.alert_request_in_progress));

        ServiceRequest serviceRequest
                = ServiceRequest.requestOrderCall(phone, performerEmail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                getHostActivity().dismissProgressDialog();

                if (response.contains("Email was sent"))
                    Toast.makeText(getContext(), getString(R.string.alert_order_has_success), Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().dismissProgressDialog();
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });

        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        requestLocation();
    }

    private void onServiceEdit(Service service) {
        buildView(getView(), service);
    }

    private void onSuccessfulActivityResult(int requestCode, Intent data) {

        if ( requestCode == REQUEST_EDIT_SERVICE ) {
            String key = data.getStringExtra(Constants.Extras.CONTAINER_SERVICE);
            onServiceEdit( (Service) Container.getInstance().pop(key) );

        } else if (requestCode == REQUEST_OPEN_GALLERY) {
            getHostActivity().showProgresDialog(getString(R.string.alert_request_in_progress));
            startService(service.getServerID(), data.getData());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
            onSuccessfulActivityResult(requestCode, data);

    }

    @Override
    public void onProgress(int progress) {

    }

    @Override
    public void onSucess(String url) {
        getHostActivity().dismissProgressDialog();

        service.addImageUrl(url);

        Image image = new Image();
        image.setUrl(url);
        image.setServiceID(service.getServerID());

        ImageRequest imageRequest = ImageRequest.requestAddImage(image, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //successfulFinish(createdService);
                ((GalleryAdapter) rvGallery.getAdapter()).setItems(service.getImages());
                Toast.makeText(getContext(), getString(R.string.alert_success_loading), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.error_fail_server_connection), Toast.LENGTH_SHORT).show();
            }
        });

        imageRequest.setTag(TAG);
        queue.add(imageRequest);
    }

    @Override
    public void onError() {
        getHostActivity().dismissProgressDialog();
        Toast.makeText(getContext(), getString(R.string.alert_fail_loading), Toast.LENGTH_SHORT).show();
    }
}
