package ru.nko.nkoservices.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import ru.nko.nkoservices.MainActivity;
import ru.nko.nkoservices.R;

public class AboutFragment extends BaseFragment<MainActivity> {


    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
        getHostActivity().getSupportActionBar().setTitle(getString(R.string.title_about));

        WebView wv = (WebView) view.findViewById(R.id.webView);
        //wv.loadDataWithBaseURL(null, "file:///android_asset/about_new.html","text/html", "UTF-8", null);
        wv.loadUrl("file:///android_asset/about.html");


    }
}
