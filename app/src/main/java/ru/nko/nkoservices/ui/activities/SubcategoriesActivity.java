package ru.nko.nkoservices.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.ui.fragments.ServiceFragment;
import ru.nko.nkoservices.ui.fragments.SubcategoriesFragment;

public class SubcategoriesActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Service name");
        }

        if (savedInstanceState != null)
            return;

        String categoryContainerKey = getIntent().getStringExtra(Constants.Extras.CONTAINER_CATEGORIES);
        int mode = getIntent().getIntExtra(Constants.Extras.MODE, -1);

        if (categoryContainerKey == null)
            throw new Error("You must send key to category in container");

        if (mode == -1)
            throw new Error("Argument mode has been a lost");

        addFragment(SubcategoriesFragment.newInstance(categoryContainerKey, mode));
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }

    public static void startSubcategoriesActivity(Context context, Category category) {
        Intent intent = new Intent(context, SubcategoriesActivity.class);
        String key = Container.getInstance().push(category);
        intent.putExtra(Constants.Extras.CONTAINER_CATEGORIES, key);
        intent.putExtra(Constants.Extras.MODE, Constants.Mode.OPEN_CATEGORY);
        context.startActivity(intent);
    }
}


