package ru.nko.nkoservices.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.ui.fragments.EditServiceFragment;
import ru.nko.nkoservices.ui.fragments.ListServicesFragment;

public class ServiceActivity extends BaseActivity {

    private final static int START_LIST = 0;
    private final static int START_ADD = 1;
    private final static int START_EDIT = 2;
    private final static int START_ERROR = -1;

    private int startMode = START_ERROR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Service name");
        }

        if (savedInstanceState != null)
            return;

        this.startMode = getIntent().getIntExtra(Constants.Extras.START_MODE, START_ERROR);

        if (startMode == START_LIST)
            addListFragment();
        else if (startMode == START_EDIT)
            addEditFragment();
        else if (startMode == START_ADD)
            addCreatorFragment();
        else
            throw new Error("StartMode parameter has been lost. Please use factory method for start activity !!!");

    }

    private void addListFragment() {
        String categoryKey = getIntent().getStringExtra(Constants.Extras.CONTAINER_MODEL);
        Category category = (Category) Container.getInstance().pop(categoryKey);

        addFragment( ListServicesFragment.newInstanceCategoryBy(category) );
    }

    private void addCreatorFragment() {
        addFragment( EditServiceFragment.newInstanceAddService() );
    }

    private void addEditFragment() {
        String serviceKey = getIntent().getStringExtra(Constants.Extras.CONTAINER_MODEL);
        Service service = (Service) Container.getInstance().pop(serviceKey);

        addFragment( EditServiceFragment.newInstanceEditService(service) );
    }


    @Override
    protected int getContainer() {
        return R.id.container;
    }

    /*public static void startDetailServiceActivity(Context context, Service service) {
        Intent intent = new Intent(context, ServiceActivity.class);

        intent.putExtra( Constants.Extras.CONTAINER_MODEL, Container.getInstance().push(service) );
        intent.putExtra( Constants.Extras.START_MODE, START_DETAIL );
        context.startActivity(intent);
    }*/

    public static void startListActivity(Context context, Category category) {
        Intent intent = new Intent(context, ServiceActivity.class);

        intent.putExtra( Constants.Extras.CONTAINER_MODEL, Container.getInstance().push(category) );
        intent.putExtra( Constants.Extras.START_MODE, START_LIST );
        context.startActivity(intent);
    }

    /*public static void startEditActivity(Context context) {
        Intent intent = new Intent(context, ServiceActivity.class);

        intent.putExtra( Constants.Extras.START_MODE, START_EDIT);
        context.startActivity(intent);
    } */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public static Intent intentForEditActivity(Context context, Service service) {
        Intent intent = new Intent(context, ServiceActivity.class);
        intent.putExtra( Constants.Extras.CONTAINER_MODEL, Container.getInstance().push(service) );
        intent.putExtra( Constants.Extras.START_MODE, START_EDIT);
        return intent;
    }

    public static Intent intentForAddActivity(Context context) {
        Intent intent = new Intent(context, ServiceActivity.class);
        intent.putExtra( Constants.Extras.START_MODE, START_ADD);
        return intent;
    }
}
