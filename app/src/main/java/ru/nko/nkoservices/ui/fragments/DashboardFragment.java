package ru.nko.nkoservices.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.MainActivity;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.requests.CategoryRequest;
import ru.nko.nkoservices.ui.activities.SubcategoriesActivity;
import ru.nko.nkoservices.ui.adapters.CategoryAdapter;
import ru.nko.nkoservices.ui.decoration.ItemOffsetDecoration;

public class DashboardFragment extends BaseFragment<MainActivity> {

    private static final String TAG = "DASHBOARD_FRAGMENT";

    private RecyclerView rvMainMenu;
    private ProgressBar progressBar;
    private RequestQueue queue;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getHostActivity().getSupportActionBar().setTitle(getString(R.string.title_categories));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        rvMainMenu = (RecyclerView) view.findViewById(R.id.rvMainMenu);
        rvMainMenu.setHasFixedSize(true);

        GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.margin_dashboard);

        requestLoadCategories();

        rvMainMenu.setLayoutManager(mLayoutManager);
        rvMainMenu.addItemDecoration(itemDecoration);
    }

    private void requestLoadCategories() {
        queue = Volley.newRequestQueue(getContext());

        CategoryRequest categoryRequest = CategoryRequest.loadCategories ( new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressBar.setVisibility(View.GONE);
                rvMainMenu.setVisibility(View.VISIBLE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    parseResponse( jsonResponse.getString(Constants.CATEGORIES) );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        categoryRequest.setTag(TAG);
        queue.add(categoryRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    private void parseResponse(String jsonCategories) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        //List<Category> list = gson.fromJson(jsonCategories, new TypeToken<ArrayList<Category>>() {}.getTypeID());
        NKOApplication.list = gson.fromJson(jsonCategories, new TypeToken<ArrayList<Category>>() {}.getType());
        List<Category> list = NKOApplication.list;
        /*List<Category> list = new ArrayList<>();
        Category category = new Category();
        category.setName("Cat 1");
        list.add(category);

        category = new Category();
        category.setName("Cat 2");
        list.add(category); */

        CategoryAdapter mainMenuAdapter = new CategoryAdapter(list, CategoryAdapter.DASH_VIEW_TYPE, getCategoryInteraction());
        rvMainMenu.setAdapter(mainMenuAdapter);
    }

    private CategoryAdapter.InteractionListener getCategoryInteraction() {
        return new CategoryAdapter.InteractionListener() {
            @Override
            public void onItemInteraction(Category category) {

                if (category.getChildren().isEmpty()) {
                    Toast.makeText(getContext(), getString(R.string.prompt_empty_category), Toast.LENGTH_SHORT).show();
                    return;
                }

                SubcategoriesActivity.startSubcategoriesActivity(getContext(), category);
            }
        };
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_search).setVisible(true);
        super.onPrepareOptionsMenu(menu);
    }
}
