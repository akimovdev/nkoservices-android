package ru.nko.nkoservices.ui.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Image;
import ru.nko.nkoservices.model.entity.MenuItem;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private List<String> items = new ArrayList<>();
    private DisplayImageOptions options;

    public GalleryAdapter(List<Image> items) {

        for (Image image : items)
            this.items.add(image.getUrl());

        //this.items.addAll(items);
        //inflater = LayoutInflater.from(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_launcher)
                //.showImageOnLoading(R.drawable.ic_stub)
                //.showImageForEmptyUri(R.drawable.ic_empty)
                //.showImageOnFail(R.drawable.ic_error)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                //.displayer(new RoundedBitmapDisplayer(20))
                .build();
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_gallery_image, parent, false);

        return new GalleryAdapter.GalleryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        ImageView imageView = (ImageView) holder.itemView;//.findViewById(R.id.image);
        ImageLoader.getInstance().displayImage(items.get(position), imageView, options);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    /*@Override
    public int getCount() {
        return IMAGE_URLS.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = (ImageView) convertView;
        if (imageView == null) {
            imageView = (ImageView) inflater.inflate(R.layout.item_gallery_image, parent, false);
        }
        ImageLoader.getInstance().displayImage(IMAGE_URLS[position], imageView, options);
        return imageView;
    } */

    public void setItems(List<Image> items) {
        this.items.clear();
        for (Image image : items)
            this.items.add(image.getUrl());

        notifyDataSetChanged();
    }

    static class GalleryViewHolder extends RecyclerView.ViewHolder {

        //TextView tvItem;

        public GalleryViewHolder (View itemView) {
            super(itemView);
            //tvItem = (TextView) itemView.findViewById(R.id.tvItem);
        }
    }
}
