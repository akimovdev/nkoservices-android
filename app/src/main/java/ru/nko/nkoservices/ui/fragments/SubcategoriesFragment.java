package ru.nko.nkoservices.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.ui.activities.ServiceActivity;
import ru.nko.nkoservices.ui.activities.SubcategoriesActivity;
import ru.nko.nkoservices.ui.adapters.CategoryAdapter;
import ru.nko.nkoservices.ui.decoration.DividerItemDecoration;

import static android.app.Activity.RESULT_OK;

public class SubcategoriesFragment extends BaseFragment<SubcategoriesActivity> {

    private static final String ARG_CATEGORY_CONTAINER_KEY = "category_container_key";
    private static final String ARG_MODE = "mode";

    private Category category;
    private RecyclerView recyclerView;
    private int mColumnCount = 1;
    private int mode = -1;

    public SubcategoriesFragment() {
        // Required empty public constructor
    }

    public static SubcategoriesFragment newInstance(String categoryContainerKey, int mode) {
        SubcategoriesFragment fragment = new SubcategoriesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY_CONTAINER_KEY, categoryContainerKey);
        args.putInt(ARG_MODE, mode);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String key = getArguments().getString(ARG_CATEGORY_CONTAINER_KEY);
            mode = getArguments().getInt(ARG_MODE, -1);

            if (key == null)
                throw new Error("You must send key to category in container");
            if (mode == -1)
                throw new Error("Argument mode has been a lost, use please factory method for run fragment");

            this.category = (Category) Container.getInstance().pop(key);
        } else {
            throw new Error("You must send key to category in container");
        }

        getHostActivity().getSupportActionBar().setTitle(category.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sucategories, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
                recyclerView.addItemDecoration( new DividerItemDecoration(context, R.drawable.divider) );
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
        }

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        CategoryAdapter mainMenuAdapter = new CategoryAdapter(category.getChildren(), CategoryAdapter.LIST_VIEW_TYPE, getCategoryInteraction());
        recyclerView.setAdapter(mainMenuAdapter);
    }

    private void openSubcategory(Category category) {
        String key = Container.getInstance().push(category);
        getHostActivity().replaceFragment(SubcategoriesFragment.newInstance(key, mode), true);
    }


    private void onUltimateCategory(Category category) {

        if (mode == Constants.Mode.SELECT_CATEGORY) {
            Intent intent = new Intent();
            intent.putExtra(Constants.Extras.CATEGORY_ID, category.getServerID());
            intent.putExtra(Constants.Extras.CATEGORY_NAME, category.getName());
            getActivity().setResult(RESULT_OK, intent);
            getActivity().finish();
        } else {
            if (NKOApplication.getInstance().isNetworkAvailable())
                ServiceActivity.startListActivity(getContext(), category);
            else
                Toast.makeText(getContext(), getString(R.string.alert_lost_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private CategoryAdapter.InteractionListener getCategoryInteraction() {
        return new CategoryAdapter.InteractionListener() {
            @Override
            public void onItemInteraction(Category category) {
                if (category.getChildren().isEmpty()) {
                    onUltimateCategory(category);
                } else {
                    openSubcategory(category);
                }
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
