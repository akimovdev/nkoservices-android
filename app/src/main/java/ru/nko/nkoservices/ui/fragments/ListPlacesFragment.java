package ru.nko.nkoservices.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Location;
import ru.nko.nkoservices.model.requests.CategoryRequest;
import ru.nko.nkoservices.model.requests.GeocodingRequest;
import ru.nko.nkoservices.ui.activities.PlacesActivity;
import ru.nko.nkoservices.ui.adapters.CategoryAdapter;
import ru.nko.nkoservices.ui.adapters.LocationAdapter;
import ru.nko.nkoservices.ui.decoration.DividerItemDecoration;

import static android.R.attr.category;
import static android.app.Activity.RESULT_OK;

public class ListPlacesFragment extends BaseFragment<PlacesActivity> implements LocationAdapter.InteractionListener {

    private static final String TAG = "LIST_PLACES_FRAGMENT";

    private RequestQueue queue;
    private ProgressBar progressBar;
    private RecyclerView rvLocations;


    public ListPlacesFragment() {
        // Required empty public constructor
    }

    public static ListPlacesFragment newInstance() {
        ListPlacesFragment fragment = new ListPlacesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_places, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        rvLocations = (RecyclerView) view.findViewById(R.id.rvList);
        rvLocations.setLayoutManager(new LinearLayoutManager(getContext()));
        rvLocations.addItemDecoration( new DividerItemDecoration(getContext(), R.drawable.divider) );

        requestLoadLocations();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (queue != null) {
            queue.cancelAll(TAG);
        }
    }

    private void parseResponse(String jsonLocations) {

        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        Gson gson = builder.create();

        //List<Category> list = gson.fromJson(jsonCategories, new TypeToken<ArrayList<Category>>() {}.getTypeID());
        //NKOApplication.list = gson.fromJson(jsonCategories, new TypeToken<ArrayList<Category>>() {}.getType());
        List<Location> list = gson.fromJson(jsonLocations, new TypeToken<ArrayList<Location>>() {}.getType());

        LocationAdapter adapter = new LocationAdapter(list, this);
        rvLocations.setAdapter(adapter);
    }

    private void requestLoadLocations() {
        queue = Volley.newRequestQueue(getContext());

        GeocodingRequest listLocationsRequest = GeocodingRequest.requestListLocations(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    parseResponse( jsonResponse.getString(Constants.LOCATIONS) );
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        });

        listLocationsRequest.setTag(TAG);
        queue.add(listLocationsRequest);
    }


    /*private LocationAdapter.InteractionListener getCategoryInteraction() {
        return new LocationAdapter.InteractionListener() {
            @Override
            public void onItemInteraction(Location location) {
                //if (category.getChildren().isEmpty()) {
                    //onUltimateCategory(category);
                //} else {
                    //openSubcategory(category);
                //}
            }
        };
    }*/

    @Override
    public void onItemInteraction(Location location) {

        Intent intent = new Intent();
        intent.putExtra(Constants.Extras.LOCATION_ID, location.getServerID());
        intent.putExtra(Constants.Extras.LOCATION_NAME, location.getName());
        intent.putExtra(Constants.Extras.LOCATION_AREA, location.getArea());
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }

    /* private static List<Location> getLocations() {

        List<Location> list = new ArrayList<>();

        Location location = new Location();
        location.setName("Location 1");
        list.add(location);

        location = new Location();
        location.setName("Location 2");
        list.add(location);

        location = new Location();
        location.setName("Location 3");
        list.add(location);

        return list;
    } */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
