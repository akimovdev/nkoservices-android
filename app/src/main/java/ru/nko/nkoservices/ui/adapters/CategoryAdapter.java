package ru.nko.nkoservices.ui.adapters;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{

    public static final int DASH_VIEW_TYPE = 0;
    public static final int LIST_VIEW_TYPE = 1;

    private List<Category> items = new ArrayList<>();
    private int viewType;
    private int colorIndex = 0;
    private int[] COLORS = new int[]{ R.color.colorPrimaryGreen,
                                        R.color.colorAccent,
                                        R.color.colorPrimary };

    private InteractionListener interactionListener;

    public CategoryAdapter(List<Category> items, int viewType, InteractionListener interactionListener) {
        this.items.addAll(items);
        this.viewType = viewType;
        this.interactionListener = interactionListener;
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = viewType == DASH_VIEW_TYPE ? R.layout.item_dash_service : R.layout.item_category;

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(layout, parent, false);

        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, int position) {
        holder.item = items.get(position);
        holder.tvItem.setText(holder.item.getName());

        if (holder.getItemViewType() == DASH_VIEW_TYPE) {

            if (colorIndex < 2)
                colorIndex++;
            else
                colorIndex = 0;

            holder.ivIcon.setColorFilter( ContextCompat.getColor(holder.itemView.getContext(), COLORS[colorIndex]) );
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != interactionListener) {
                    interactionListener.onItemInteraction(holder.item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView ivIcon;
        TextView tvItem;
        Category item;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
            tvItem = (TextView) itemView.findViewById(R.id.tvItem);

        }
    }

    public interface InteractionListener {
        void onItemInteraction(Category category);
    }

    /*public static List<MenuItem> createDebugItemList() {
        List<MenuItem> items = new ArrayList<>();

        items.add(new MenuItem("Service 1"));
        items.add(new MenuItem("Service 2"));
        items.add(new MenuItem("Service 3"));
        items.add(new MenuItem("Service 4"));
        items.add(new MenuItem("Service 5"));
        items.add(new MenuItem("Service 6"));
        items.add(new MenuItem("Service 7"));
        items.add(new MenuItem("Service 8"));

        return items;
    } */

}
