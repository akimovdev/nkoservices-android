package ru.nko.nkoservices.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.Container;
import ru.nko.nkoservices.NKOApplication;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.model.entity.User;
import ru.nko.nkoservices.model.requests.ServiceRequest;
import ru.nko.nkoservices.ui.activities.AuthActivity;
import ru.nko.nkoservices.ui.activities.BaseActivity;
import ru.nko.nkoservices.ui.activities.DetailServiceActivity;
import ru.nko.nkoservices.ui.activities.ServiceActivity;
import ru.nko.nkoservices.ui.adapters.ServiceAdapter;
import ru.nko.nkoservices.ui.decoration.DividerItemDecoration;

import static android.app.Activity.RESULT_OK;

public class ListServicesFragment extends BaseFragment<BaseActivity> {

    private static final String TAG = "LIST_SERVICES_FRAGMENT";

    private static final int REQUEST_AUTH = 1;
    private static final int REQUEST_ADD_SERVICE = 2;

    private static final String ARG_TYPE_START = "type_start";
    private static final String ARG_ID = "id";
    private static final String ARG_NAME = "name";
    private static final String ARG_QUERY = "query";

    private static final int START_OWNER_BY = 0;
    private static final int START_CATEGORY_BY = 1;
    private static final int START_SEARCH = 2;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private int mColumnCount = 1;
    private TextView tvEmpty;

    private long id = -1;
    private int startType = -1;
    private String query = null;
    private RequestQueue queue;
    private Service selectedItem;
    private String title;

    public ListServicesFragment() {

    }

    public static ListServicesFragment newInstanceSearch(String query) {
        ListServicesFragment fragment = new ListServicesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE_START, START_SEARCH);
        args.putString(ARG_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }

    public static ListServicesFragment newInstanceCategoryBy(Category category) {
        ListServicesFragment fragment = new ListServicesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE_START, START_CATEGORY_BY);
        args.putLong(ARG_ID, category.getServerID());
        args.putString(ARG_NAME, category.getName());
        fragment.setArguments(args);
        return fragment;
    }

    public static ListServicesFragment newInstanceOwnerBy(User user) {
        ListServicesFragment fragment = new ListServicesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE_START, START_OWNER_BY);
        args.putLong(ARG_ID, user.getServerID());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null) {
            throw new Error("ServiceFragment can not be start without arguments. Use please factory method for start ServiceFragment !!!");
        }

        startType = getArguments().getInt(ARG_TYPE_START, -1);
        title = getString(R.string.title_own_category);

        if (startType == START_SEARCH)
            query = getArguments().getString(ARG_QUERY, null);
        else if ( startType == START_CATEGORY_BY ) {
            id = getArguments().getLong(ARG_ID, -1);
            title = getArguments().getString(ARG_NAME);
        } else if ( startType == START_OWNER_BY ) {
            id = getArguments().getLong(ARG_ID, -1);
        } else {
            throw new Error("Start type parameter has been lost");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);

        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.rvList);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.addItemDecoration( new DividerItemDecoration(context, R.drawable.divider) );
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();

        if (queue != null)
            queue.cancelAll(TAG);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        getHostActivity().getSupportActionBar().setTitle(title);

        queue = Volley.newRequestQueue(getContext());

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        tvEmpty = (TextView) view.findViewById(R.id.tvEmpty);

        if (startType == START_OWNER_BY)
            requestOwnerBy();
        else if (startType == START_CATEGORY_BY) {
            requestListCategoryBy();
        } else if (startType == START_SEARCH) {
            requestListNameBy(query);
        }
    }

    public void onSearchQuery(String query) {
        ServiceAdapter adapter = (ServiceAdapter) recyclerView.getAdapter();
        adapter.clearAll();
        progressBar.setVisibility(View.VISIBLE);
        requestListNameBy(query);
    }

    Response.Listener<String> getOnResponseListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                    Gson gson = builder.create();
                    List<Service> list = gson.fromJson(jsonResponse.getString(Constants.SERVICES), new TypeToken<ArrayList<Service>>() {}.getType());

                    if (list.isEmpty())
                        tvEmpty.setVisibility(View.VISIBLE);

                    recyclerView.setAdapter( new ServiceAdapter(list, getItemListener()) );

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


            }
        };
    }

    Response.ErrorListener getOnErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), getString(R.string.alert_fail_connection), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void requestListNameBy(String query) {
        //queue = Volley.newRequestQueue(getContext());
        ServiceRequest serviceRequest = ServiceRequest.requestListNameBy(query, getOnResponseListener(), getOnErrorListener());
        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);
    }

    private void requestListCategoryBy() {
        ServiceRequest serviceRequest = ServiceRequest.requestListCategoryBy(id, getOnResponseListener(), getOnErrorListener());
        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);
    }

    private void requestOwnerBy() {
        ServiceRequest serviceRequest = ServiceRequest.requestListOwnerBy(id, getOnResponseListener(), getOnErrorListener());
        serviceRequest.setTag(TAG);
        queue.add(serviceRequest);
    }

    private void openAuthActivity(Service item) {
        selectedItem = item;
        Intent intent = new Intent(getContext(), AuthActivity.class);
        startActivityForResult(intent, REQUEST_AUTH);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
            onSuccessfulActivityResult(requestCode, data);


    }

    private OnListFragmentInteractionListener getItemListener() {
        return new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(Service item) {

                if (NKOApplication.getInstance().getCurrentUser() == null) {
                    openAuthActivity(item);
                    return;
                }

                if (!NKOApplication.getInstance().isNetworkAvailable()) {
                    Toast.makeText(getContext(), getString(R.string.alert_lost_internet_connection), Toast.LENGTH_SHORT).show();
                    return;
                }

                DetailServiceActivity.startDetailServiceActivity(getContext(), item );
            }
        };
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        if (startType == START_OWNER_BY)
            menu.findItem(R.id.action_add).setVisible(true);
        else if (startType == START_SEARCH)
            menu.findItem(R.id.action_search).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                break;

            case R.id.action_add:
                startEditActivityForResult();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onServiceAdded(Service service) {
        ServiceAdapter adapter = (ServiceAdapter) recyclerView.getAdapter();
        adapter.addItem(service);

        recyclerView.scrollToPosition(adapter.getItemCount());
    }

    private void onSuccessfulActivityResult(int requestCode, Intent data) {

        if ( requestCode == REQUEST_AUTH ) {
            boolean result = data.getBooleanExtra(Constants.Extras.AUTH_RESULT, false);
            if (result) DetailServiceActivity.startDetailServiceActivity(getContext(), selectedItem );

        } else if ( requestCode == REQUEST_ADD_SERVICE ) {
            Toast.makeText(getContext(), getString(R.string.alert_order_has_success), Toast.LENGTH_SHORT).show();
            String key = data.getStringExtra(Constants.Extras.CONTAINER_SERVICE);
            onServiceAdded( (Service) Container.getInstance().pop(key) );
        }
    }

    private void startEditActivityForResult() {
        Intent intent = ServiceActivity.intentForAddActivity(getContext());
        startActivityForResult(intent, REQUEST_ADD_SERVICE);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Service item);
    }
}
