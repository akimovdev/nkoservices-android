package ru.nko.nkoservices.ui.adapters;

import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nko.nkoservices.Constants;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Service;
import ru.nko.nkoservices.ui.fragments.ListServicesFragment.OnListFragmentInteractionListener;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private final List<Service> mValues;
    private final OnListFragmentInteractionListener mListener;
    private DisplayImageOptions avatarOptions;
    private int bgIndex = 0;

    private int[] BACKGROUND = new int[]{ R.drawable.blue_background,
            R.drawable.orange_background,
            R.drawable.green_background };

    public ServiceAdapter(List<Service> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;

        avatarOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                //.showImageOnFail(R.drawable.no_avatar)
                //.showImageForEmptyUri(R.drawable.no_avatar)
                //.showImageOnLoading(R.drawable.no_avatar)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_service, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        if (holder.mItem.getImages() != null && !holder.mItem.getImages().isEmpty()) {
            holder.ivIcon.setVisibility(View.GONE);
            ImageLoader.getInstance()
                    .displayImage(holder.mItem.getImages().get(0).getUrl(), holder.civAvatar, avatarOptions);
        } else {
            holder.ivIcon.setVisibility(View.VISIBLE);
            holder.civAvatar.setImageDrawable(ContextCompat.getDrawable(holder.itemView.getContext(), BACKGROUND[bgIndex]));
        }

        holder.tvName.setText(mValues.get(position).getName());

        if (mValues.get(position).getTypeID() == Service.TYPE_FREE)
            holder.tvCost.setText(holder.itemView.getContext().getString(R.string.free));
        else
            holder.tvCost.setText(mValues.get(position).getCost()+Constants.SYMBOL_RUB);

        holder.tvLocation.setText(mValues.get(position).getLocation().getName());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

        if (bgIndex < 2)
            bgIndex++;
        else
            bgIndex = 0;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addItem(Service service) {
        mValues.add(service);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final CircleImageView civAvatar;
        public final ImageView ivIcon;
        public final TextView tvName;
        public final TextView tvCost;
        public final TextView tvLocation;
        public Service mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvCost = (TextView) view.findViewById(R.id.tvCost);
            tvLocation = (TextView) view.findViewById(R.id.tvLocation);
            civAvatar = (CircleImageView) view.findViewById(R.id.ivAvatar);
            ivIcon = (ImageView) view.findViewById(R.id.ivIcon);

        }
    }

    public void clearAll() {
        mValues.clear();
    }

}
