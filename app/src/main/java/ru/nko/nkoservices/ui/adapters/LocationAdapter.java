package ru.nko.nkoservices.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nko.nkoservices.R;
import ru.nko.nkoservices.model.entity.Category;
import ru.nko.nkoservices.model.entity.Location;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private final List<Location> mValues;
    private InteractionListener interactionListener;

    public LocationAdapter(List<Location> items, InteractionListener interactionListener) {
        mValues = items;
        this.interactionListener = interactionListener;
    }

    /*public CategoryAdapter(List<Category> items, int viewType, CategoryAdapter.InteractionListener interactionListener) {
        this.items.addAll(items);
        this.viewType = viewType;
        this.interactionListener = interactionListener;
    } */

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_location, parent, false);

        return new LocationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvName.setText(mValues.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != interactionListener) {
                    interactionListener.onItemInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView tvName;
        public Location mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvName = (TextView) view.findViewById(R.id.tvName);
        }
    }

    public interface InteractionListener {
        void onItemInteraction(Location location);
    }
}
