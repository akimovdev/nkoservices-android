package ru.nko.nkoservices.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;


public abstract class BaseActivity extends AppCompatActivity {

    //private ProgressPopupWindow popupWindow;
    private ProgressDialog progressDialog;

    public void addFragment(Fragment fragment) {

        getSupportFragmentManager().beginTransaction()
                .add(getContainer(), fragment).commit();

    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(getContainer(), fragment);

        if (addToBackStack)
            transaction.addToBackStack( fragment.getClass().getName() );

        transaction.commit();
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(getContainer());
    }


    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    protected abstract int getContainer();

    public void showAlertDialog(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    public ProgressDialog showProgresDialog(String message) {
        progressDialog = ProgressDialog.show(this, null, message, true);
        return progressDialog;
    }

    public void dismissProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //hideProgressDialog();
    }

    /*public void showProgressDialog() {
        popupWindow = ProgressPopupWindow.create(this);
        popupWindow.showAtLocation(Gravity.CENTER, 0, 0);
        popupWindow.setOutsideTouchable(false);
    }

    public void hideProgressDialog() {
        if (popupWindow == null)
            return;

        popupWindow.dismiss();
        popupWindow = null;
    } */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public interface OnFragmentBackListener {
        boolean onBackPressed();
    }

}
